# Agaric's Flagship Site

See also [SERVICES.md](https://gitlab.com/agaric/sites/agaric-com/blob/master/SERVICES.md)

## Get submodules

This project includes the [Agaric styleguide](https://gitlab.com/agaric/patternlibrary) as a [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

### When cloning

```
git clone --recurse-submodules git@gitlab.com:agaric/sites/agaric-com.git
```

### If you cloned without `--recurse-submodules`

If in your initial clone you didn't do as above, you can get the styleguide with:

```
git submodule init
git submodule update
```

### Get started

To be able to import the configuration, you need to get the initializing database (where we will also be collaborating on content) [from the test site](https://agaric-test.drutopia.org/) pending resolution of [drutopia#216](https://gitlab.com/drutopia/drutopia/issues/216).

```
ddev start
ddev auth ssh
ddev composer install
ddev . drush -y sql-dump > /tmp/paranoia.sql && drush sql-drop && drush -y sql-sync @live @self && drush -y updb
ddev . drush -y rsync @live:%files @self:%files
```

### Get updates

```
git pull
git submodule update --recursive --remote
ddev start
ddev composer install
ddev . drush -y sql-dump > /tmp/paranoia.sql && drush -y sql-drop && drush -y sql-sync @live @self && drush -y updb
ddev . drush -y rsync @live:%files @self:%files
ddev . drush cim -y
```

## Theming

See `web/themes/custom/agarica/patternlibrary/README.md` for how to make changes, especially involving Sass to CSS compilation for styles.

After making changes withing the Pattern Library within the custom Agaric theme, commit these changes *first* to the pattern library itself, and second commit the pattern library as a whole to the main project (it will be a single change, "Subproject commit" and the current hash value of the subproject repository.

## Deployment

Agaric is currently using a Platform as a Service version of Drutopia with additional modules.

Set up [drutopia_host](https://gitlab.com/drutopia-platform/drutopia_host) and [hosting_private](https://gitlab.com/drutopia-platform/hosting_private), as documented in hosting private.

Then use [ahoy](https://github.com/ahoy-cli/ahoy/), from within the hosting_private directory (`cd ~/Projects/drutopia-platform/drutopia_host/hosting_private`).

Ensure all three related repositories are up-to-date with:

```
cd ~/Projects/drutopia-platform/drutopia_host/hosting_private
ahoy git-pull-all
```

If Agaric's Drutopia PaaS base has changed (the composer.lock in this directory), produce and push a new build:

```
ahoy deploy-build agaric
```

To deploy this build to an instance of the site plus config, templates, styles:

```
ahoy deploy-site agaric_test
```

(And likewise for `agaric_live`.) Use `deploy-site-force` in place of `deploy-site` if you need to override configuration changes made on live and you are certain you have captured all wanted changes.

Then record the deployment, logs from both deploy-site and deploy-build:

```
ahoy artifacts
```

### Drupal settings file management

The Drupal settings files are also managed by Drutopia. On each deploy, the file is generated and replaces the `settings.local.php` that is on the server with whatever settings will match the Drutopia configuration. Therefore, in order to add settings, you must edit the vault settings using:

```
ahoy vault-edit
```

Look for the Yaml section for `agaric_live`, or `agaric_test`, as appropriate, and edit the `php_settings_code` section to add items to that site's setting overrides. For example:

```
      ...
      online: True
      php_settings_code: |2
        $settings['super_secret'] = p@ssw0rd;
        $config['life.answer'] = 42;
      server_aliases:
        - agaric.com
      ...
```

## Code Hightlight.

The [prism.js](https://prismjs.com/) library was added in the site and can be used like this:

```
<pre>
  <code class="language-css">
    p { color: red }
  </code>
</pre>
```
The suffix of the class needs to be changed depending of the language that is going to be highlighted eg: `language-php`,  `language-js` etc.
