def set_overrides_per_project(config)
  # Acquire a hostname from the project root
  project_dir_name = File.basename(ENV['DRUPALVM_PROJECT_ROOT']).gsub("_", "-").gsub(" ", "-")
  host_name = "#{project_dir_name}.test"

  # Override all vconfig values with the new names
  config['vagrant_hostname'] = host_name
  config['vagrant_machine_name'] = "#{project_dir_name}-vm"
  config['drupal_domain'] = host_name
  config['hostname_fqdn'] =
  config['apache_vhosts'].each do |host|
    host['servername'].sub!('drupalvm.test', host_name)
    host['serveralias'].gsub!('drupalvm.test', host_name) if host['serveralias']
  end
end
