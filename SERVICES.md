# Services

Our server is elizabeth.mayfirst.org which is named after [Elizabeth "Betita" Martínez](https://en.wikipedia.org/wiki/Elizabeth_Martínez)

## Drupal Planet

Agaric is source ID 217 and is listed as 'Agaric Collective':

https://www.drupal.org/aggregator/sources/217

The feed URL they are reading is:

https://agaric.coop/drupal-planet

Note that this does not have 'rss' or 'feed' in the path so will not match any path alias pattern we create, but it is essential we maintain this alias because that is the URL Drupal Planet is reading.
