<?php

use Drupal\block\Entity\Block;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
* Implements hook_preprocess_node() for NODE document templates.
*/
function agarica_preprocess_node(&$variables) {
  // Allowed view modes
  $view_mode = $variables['view_mode']; // Retrieve view mode
  $allowed_view_modes = ['full']; // Array of allowed view modes (for performance so as to not execute on unneeded nodes)

  // If view mode is in allowed view modes list, pass to agarica_add_regions_to_node()
  if(in_array($view_mode, $allowed_view_modes)) {
    // Allowed regions (for performance so as to not execute for unneeded region)
    $allowed_regions = ['secondary_menu'];
    agarica_add_regions_to_node($allowed_regions, $variables);
  }
}

/**
* THEME_add_regions_to_node
*/

function agarica_add_regions_to_node($allowed_regions, &$variables) {
  // Retrieve active theme
  $theme = \Drupal::theme()->getActiveTheme()->getName();

  // Retrieve theme regions
  $available_regions = system_region_list($theme, 'REGIONS_ALL');

  // Validate allowed regions with available regions
  $regions = array_intersect(array_keys($available_regions), $allowed_regions);

  // For each region
  foreach ($regions as $key => $region) {

    // Load region blocks
    $blocks = Drupal::entityTypeManager()
      ->getStorage('block')
      ->loadByProperties(['theme' => $theme, 'region' => $region]);

    // Sort ‘em
    uasort($blocks, 'Drupal\block\Entity\Block::sort');

    // Capture viewable blocks and their settings to $build
    $build = [];
    foreach ($blocks as $key => $block) {
      if ($block->access('view')) {
        $view_builder = \Drupal::entityTypeManager()
          ->getViewBuilder($block
          ->getEntityTypeId());
        $build[$key] = $view_builder
          ->view($block, 'block');
      }
    }

    // Add build to region
    $variables[$region] = $build;
  }
}

/**
 * Implements hook_preprocess_region__footer().
 */
function agarica_preprocess_region__footer(&$variables) {
  $block = Block::load('languageswitcher');
  $block_content = \Drupal::entityTypeManager()
    ->getViewBuilder('block')
    ->view($block);
  $variables['language_switcher'] = $block_content;
  $variables['language_switcher_link'] = agarica_languague_switch_link();
}

/**
 * Provides a link to switch to another language.
 */
function agarica_languague_switch_link() {
  $derivative_id = 'language_interface';
  $route = \Drupal::getContainer()->get('path.matcher')->isFrontPage() ? '<front>' : '<current>';
  $current_language = \Drupal::languageManager()->getCurrentLanguage($derivative_id)->getId();
  $links = \Drupal::languageManager()->getLanguageSwitchLinks($derivative_id, Url::fromRoute($route))->links;
  $links['en']['title'] = 'Browse this site in English';
  $links['es']['title'] = 'Consulta este sitio en Español';
  $lid = ($current_language == 'en') ? 'es' : 'en';
  // Remove current language from the links
  unset($links[$current_language]);

  // Check if the current path  display an entity.
  $entity = [];
  foreach (\Drupal::routeMatch()->getParameters() as $param) {
    if ($param instanceof EntityInterface) {
      $entity[] = $param;
    }
  }

  // Remove links if the content hasnt' been translated yet.
  if ($entity) {
    $has_translation = (method_exists($entity[0], 'getTranslationStatus')) ? $entity[0]->getTranslationStatus($lid) : FALSE;
    $this_translation = ($has_translation && method_exists($entity[0], 'getTranslation')) ? $entity[0]->getTranslation($lid) : FALSE;
    $access_translation = ($this_translation && method_exists($this_translation, 'access') && $this_translation->access('view')) ? TRUE : FALSE;
    if (!$this_translation || !$access_translation || $current_language == $lid) {
      unset($links[$lid]);
    }
  }

  $links_render_array = [];
  // Transform the links to arrays that can be renderer.
  foreach ($links as $language => $link) {
    if (isset($link['url'])) {
      $links_render_array[] = [
        '#type' => 'link',
        '#url' => $link['url'],
        '#title' => $link['title'],
        '#options' => [
          'language' => $link['language'],
        ],
      ];
    } else {
      // If there's no URL, we take only the title and don't try to make a link.
      $links_render_array[] = [
        '#type' => 'markup',
        '#markup' => $link['title'],
      ];
    }
  }

  return $links_render_array;
}
