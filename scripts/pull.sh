#!/bin/bash
set -e
# Confirmed with echo `pwd` that as long as we use via composer it's always in
# /var/www/html (aka the project root).
proj_dir="$(cd $(dirname ${0})/../ && pwd)" # Parent of this script folder.
[ "$proj_dir" != "/var/www/html" ] && { echo "Script running from unexpected path - are you running within ddev, as you should?"; exit 1; }
backup_file="$(date +%Y%m%dT%H%M%S)_pre_pull.sql"
[ ! -d /var/www/html/backups ] && mkdir /var/www/html/backups || true
echo "Backing up current db to backups/${backup_file}..."
drush sql-dump > /var/www/html/backups/${backup_file}
echo "Dropping local db..."
drush -y sql-drop
echo "Pulling db from live into local..."
drush -y sql-sync @live @self
echo "Pulling files from live into local..."
drush -y rsync @live:%files @self:%files
drush cr
echo "Done"
