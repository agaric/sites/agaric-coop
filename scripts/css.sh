#!/bin/bash
set -e
# Confirmed with echo `pwd` that as long as we use via composer it's always in
# /var/www/html (aka the project root).
proj_dir="$(cd $(dirname ${0})/../ && pwd)" # Parent of this script folder.
[ "$proj_dir" != "/var/www/html" ] && { echo "Script running from unexpected path - are you running within ddev, as you should?  Try ddev composer css"; exit 1; }
cd /var/www/html/web/themes/custom/agarica/patternlibrary
sassc agaric/sass/agaric.scss agaric/css/agaric.css
echo "Don't forget to commit any updates in the submodule web/themes/custom/agarica/patternlibrary"
